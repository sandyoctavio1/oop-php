<?php
require_once('animal.php');
require_once('ape.php');
require_once('frog.php');

$binatang = new Animal("Shaun the sheep");
echo "Nama Binatang = " . $binatang->name . "<br>";
echo "Jumlah Kaki = " . $binatang->legs . "<br>";
echo "Binatang Berdarah Dingin? = " . $binatang->cold_blooded . "<br> <br>";

$koncek = new Frog("Buduk");
echo "Nama Binatang = " . $koncek->name . "<br>";
echo "Jumlah Kaki = " . $koncek->legs . "<br>";
echo "Binatang Berdarah Dingin? = " . $koncek->cold_blooded . "<br>";
echo $koncek->jump()."<br><br>";

$kera = new Ape("kera sakti udud");
echo "Nama Binatang = " . $kera->name . "<br>";
echo "Jumlah Kaki = " . $kera->legs . "<br>";
echo "Binatang Berdarah Dingin? = " . $kera->cold_blooded . "<br>";
echo $kera->yell();



?>