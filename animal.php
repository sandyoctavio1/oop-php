<?php

class Animal {
    public $nama;
    public $legs = 4;
    public $cold_blooded = "no";
    
    public function __construct($string){
        $this->name= $string;
    }

    /*
$sheep = new Animal("shaun");

echo $sheep->name; // "shaun"
echo $sheep->legs; // 4
echo $sheep->cold_blooded; // "no"
*/
// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())


/*
Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. 
Masing-masing class dibuat ke dalam dua file Frog.php & Ape.php. 
Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat jumlah kaki 4. 
class Ape memiliki function yell() yang mengeprint “Auooo” dan class Frog memiliki function jump() 
yang akan mengeprint “hop hop”.

*/


}


?>
