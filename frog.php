<?php
/*
Buatlah class Frog dan class Ape yang merupakan inheritance dari class Animal. 
Masing-masing class dibuat ke dalam dua file Frog.php & Ape.php. 
Perhatikan bahwa Ape (Kera) merupakan hewan berkaki 2, hingga dia tidak menurunkan sifat jumlah kaki 4. 
class Ape memiliki function yell() yang mengeprint “Auooo” dan class Frog memiliki function jump() 
yang akan mengeprint “hop hop”.

*/


require_once('animal.php');
class Frog extends Animal{
    
    public function jump(){
        echo "Jump = Hop Hoppp Hop Hulaaaa";
    }

}


?>